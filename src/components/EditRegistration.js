import React, {Component} from 'react';
import {Modal, Button, Row, Col, Form} from 'react-bootstrap';

export class EditRegistration extends Component {
    constructor(props) {
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit(event) {   
        event.preventDefault();
        fetch('http://localhost:3001/api/registration', {
            method: 'PUT',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                id: event.target.Id.value,
                first: event.target.First.value,
                last: event.target.Last.value,
                age: event.target.Age.value
            })
        })
        .then(res => res.json())
        .then((result) =>
            {
                alert(result.message);
            },
            (error) => {
                alert('Error in connecting to the database');
            }
            );
     }

    render() {
        return (
            <Modal
            {...this.props}
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered
          >
            <Modal.Header closeButton>
              <Modal.Title id="contained-modal-title-vcenter">
                Edit registration
              </Modal.Title>
            </Modal.Header>
            <Modal.Body>
            <div className="container">
             <Row>
                <Col sm={8}>
                    <Form onSubmit={this.handleSubmit}>
                        <Form.Group controlId="Id" >
                            <Form.Label>Id</Form.Label>
                            <Form.Control
                            type="text"
                            name="Id"
                            required
                            disabled
                            defaultValue={this.props.id}
                            placeholder="Id"
                            />
            
                        </Form.Group>
                        <Form.Group controlId="First">
                            <Form.Label>First name</Form.Label>
                            <Form.Control
                            type="text"
                            name="First"
                            required
                            defaultValue={this.props.first}
                            placeholder="First"
                            />
            
                        </Form.Group>
                        <Form.Group controlId="Last">
                            <Form.Label>Last name</Form.Label>
                            <Form.Control
                            type="text"
                            name="Last"
                            required
                            defaultValue={this.props.last}
                            placeholder="Last"
                            />
            
                        </Form.Group>
                        <Form.Group controlId="Age">
                            <Form.Label>Age</Form.Label>
                            <Form.Control
                            type="text"
                            name="Age"
                            required
                            defaultValue={this.props.age}
                            placeholder="Age"
                            />
            
                        </Form.Group>
                        <Form.Group>
                            <Button variant='primary' type='submit'>Edit registration</Button>
                        </Form.Group>
                        
                    </Form>
                </Col>
            </Row>
             </div>
            </Modal.Body>
            <Modal.Footer>
              <Button onClick={this.props.onHide}>Close</Button>
            </Modal.Footer>
          </Modal>
        )
    }
}

export default EditRegistration;