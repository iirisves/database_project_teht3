import React, {Component} from 'react';

export class Home extends Component {
    constructor(props) {
        super(props);
    }

    render(){
        return (
            <div>
                <h1>Home</h1>
                <p>This website connects via a REST API to a database with CRUD (create, read, update, delete) functionality.</p>
                <p>This app was made using Node.js, ExpressJS, React, Bootstrap and MySQL.</p>
                <p>Made by Iiris Vesala. <a href="https://gitlab.com/iirisves/database_project_teht3">GitLab repository</a></p>
            </div>
        );
    }
}

export default Home;