import React, {Component} from 'react';
import {Table, ButtonToolbar, Button} from 'react-bootstrap';
import {AddRegistration} from './AddRegistration';
import {EditRegistration} from './EditRegistration';
import './Registrations.css';

export class Registrations extends Component {

    constructor(props) {
        super(props);
        this.state={regs : [], addModalShow:false, editModalShow:false}
    }

    componentDidMount() {
        this.refreshList();
    }

    refreshList() {
        fetch('http://localhost:3001/api/registrations', {
            method: 'GET'
        })
        .then(response => response.json())
        .then((result) => {
       this.setState({regs: result.data})})
            .catch(err => alert('Error in connecting to the database'))
    }

    componentDidUpdate(prevProps, prevState) {
        // NB: If you call refreshList() without any condition checking, you will cause an infinite loop
        // Here the condition is whether either of the modal windows has just been opened or closed
        // deleteRegistration() also calls refreshList()
        if ((prevState.addModalShow !== this.state.addModalShow) || (prevState.editModalShow !== this.state.editModalShow)) {
            this.refreshList();
        }
    }

    deleteRegistration(id){
        if (window.confirm('Are you sure you want to delete this registration?')) {
            fetch('http://localhost:3001/api/registration/' + id, {
                method: 'DELETE',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
                }
                )
        }
        this.refreshList();
    }

    render() {
        const {regs, id, first, last, age} = this.state;
        let addModalClose=()=>this.setState({addModalShow:false});
        let editModalClose=()=>this.setState({editModalShow:false});
        return (
            <div>
              <ButtonToolbar>
              <Button
                  className='addButton'
                variant='info'
                onClick={()=>this.setState({addModalShow:true})}
                >+ Add registration</Button>
                <AddRegistration
                show={this.state.addModalShow}
                onHide={addModalClose}
                />
            </ButtonToolbar>
               <Table className="registrationsTable" bordered centered>
                   <thead>
                    <tr>
                        <th>Id</th>
                        <th>First name</th>
                        <th>Last name</th>
                        <th>Age</th>
                        <th>Options</th>
                    </tr>
                   </thead>
                   <tbody>
                    {regs.map(reg=>
                        <tr key = {reg.id}>
                        <td>{reg.id}</td>
                        <td>{reg.first}</td>
                        <td>{reg.last}</td>
                        <td>{reg.age}</td>
                        <td>
                            <ButtonToolbar style={{display: 'block'}}>
                                <Button className='mr-2' variant='info' onClick= {() => this.setState({editModalShow:true, id: reg.id, first: reg.first, last: reg.last, age: reg.age })}>
                                Edit
                                </Button>
                                <EditRegistration
                                show={this.state.editModalShow}
                                onHide={editModalClose}
                                id={id}
                                first={first}
                                last={last}
                                age={age}
                                />
                                <Button className='mr-2' variant='danger' onClick= {() => this.deleteRegistration(reg.id)}>
                                    Delete
                                    </Button>
                            </ButtonToolbar>
                            </td>
                        </tr>
                    )
                    }
                   </tbody>
               </Table> 
            </div>
        )
    }
}

export default Registrations;