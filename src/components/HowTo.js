import React, {Component} from 'react';
import {ButtonGroup, Button} from 'react-bootstrap';

export class HowTo extends Component {
    constructor(props) {
        super(props);
    }

    createTable(){
        fetch('http://localhost:3001/api/createtable', {
            method: 'GET'
        })
            .then(res => res.json())
            .then((result) =>
                {
                    alert(result.message);
                },
                (error) => {
                    alert('Error. Make sure you haven\'t created the table already.');
                }
            )
    }

    addData(){
        fetch('http://localhost:3001/api/adddata', {
            method: 'GET'
        })
            .then(res => res.json())
            .then((result) =>
                {
                    alert(result.message);
                },
                (error) => {
                    alert('Error.');
                }
            )
    }

    render() {
        return (
            <div>
                <h1>How to create the database</h1>
                <h2>Install MySQL on your local computer</h2>
                <p>Make sure you have installed MySQL (or a free fork of SQL, like MariaDB) and a MySQL client (like HeidiSQL, which comes bundled together with the MariaDB download for Windows) on your computer.</p>
                <a href="https://downloads.mariadb.org/">Download MariaDB + HeidiSQL</a>
                <h2>Connect to your local MySQL session as root user</h2>
                <p>Connect to your MySQL client locally on user = root, with no password (i.e. password = ' ').</p>
                <p>This is because these are the connection details hardcoded in file server.js. This app is for demo purposes only and similar code should not be used in production.</p>
                <p>The server won't run before you have created a database called "db" as the root user. You can do this through HeidiSQL.</p>
                <p>After creating the database on your computer, click these buttons in the following order to create the table on your computer.</p>
                <ButtonGroup vertical>
                <Button className='m-2' onClick= {() => this.createTable()}>
                    Step 1: Create table
                </Button>
                <Button className='m-2' onClick= {() => this.addData()}>
                    Step 2: Populate table
                </Button>
                </ButtonGroup>
                <p>Now you can navigate to <a href="/registrations">Registrations</a> on the upper menu to see your database and edit it through this app.</p>
            </div>
        );
    }
}

export default HowTo;