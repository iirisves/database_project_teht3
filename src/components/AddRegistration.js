import React, {Component} from 'react';
import {Modal, Button, Row, Col, Form} from 'react-bootstrap';

export class AddRegistration extends Component {

    constructor(props) {
        super(props);
        this.state={id: '', first: '', last: '', age: ''};
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit(event) {   
        event.preventDefault();
        fetch('http://localhost:3001/api/registration', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                first: event.target.First.value,
                last: event.target.Last.value,
                age: event.target.Age.value
            })
        })
        .then(res => res.json())
        .then((result) =>
            {
                alert(result.message);
            },
            (error) => {
                alert('Error in connecting to the database');
            }
            )
     }

    render() {
        return (
            <Modal
            {...this.props}
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered
          >
            <Modal.Header closeButton>
              <Modal.Title id="contained-modal-title-vcenter">
                Add registration
              </Modal.Title>
            </Modal.Header>
            <Modal.Body>
            <div className="container">
             <Row>
                <Col sm={8}>
                    <Form onSubmit={this.handleSubmit}>
                        <Form.Group controlId="First">
                            <Form.Label>First name</Form.Label>
                            <Form.Control
                            type="text"
                            name="First"
                            required
                            placeholder="e.g. Mickey"
                            />
            
                        </Form.Group>
                        <Form.Group controlId="Last">
                            <Form.Label>Last name</Form.Label>
                            <Form.Control
                            type="text"
                            //id="Last"
                            name="Last"
                            required
                            placeholder="e.g. Mouse"
                            />
            
                        </Form.Group>
                        <Form.Group controlId="Age">
                            <Form.Label>Age</Form.Label>
                            <Form.Control
                            type="text"
                            //id="Age"
                            name="Age"
                            required
                            placeholder="e.g. 92"
                            />
            
                        </Form.Group>
                        <div>
                        <Form.Group>
                            <Button variant='primary' type='submit'>Add registration</Button>
                        </Form.Group>
                        </div>
                    </Form>
                </Col>
            </Row>
             </div>
            </Modal.Body>
            <Modal.Footer>
              <Button onClick={this.props.onHide}>Close</Button>
            </Modal.Footer>
          </Modal>
        )
    }
}

export default AddRegistration;