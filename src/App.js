import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter, Route, Switch } from "react-router-dom";
import Registrations from './components/Registrations';
import Home from './components/Home';
import {TopMenu} from './components/TopMenu';
import HowTo from './components/HowTo';

class App extends Component {
  render() {
    return (
      <BrowserRouter>
      <TopMenu/>
        <Switch>
          <Route path="/" component={Home} exact/>
          <Route path='/registrations' component={Registrations} />
          <Route path='/howto' component={HowTo} />
        </Switch>
        </BrowserRouter>

    );
  }
}

export default App;
