const express = require('express');
const mysql = require('mysql');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const cors = require('cors');

const app = express();
app.use(morgan('combined'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(cors());

//Create connection
const db = mysql.createConnection({
    host     : 'localhost',
    user     : 'root',
    password : '',
    database : 'db'
});

//Connect
db.connect((error) => {
    if(error) {
        throw error;
    }
    console.log('MySQL connected.');
});


//Create database through the app
//You cannot connect to the server until this is done, so this is a bit redundant
/* app.get('/api/createdb', (req, res)=> {
    let sql = 'CREATE DATABASE db';

    db.query(sql, (error, result) => {
        if(error) throw error;
        res.send({message: 'Database created...'});
    });
});
 */

//Create table
app.get('/api/createtable', (req, res)=> {
    let sql = 'CREATE TABLE registration(id int not null AUTO_INCREMENT PRIMARY KEY, first varchar(255), last varchar(255), age int)';

    db.query(sql, (error, result) => {
        if (error) throw error;
        res.send({message: 'Table created.'});
    });
});

//Insert dummy data into table
app.get('/api/adddata', (req, res)=> {
    const values = [['Aku', 'Ankka', 45], ['Iines', 'Ankka', 43], ['Roope', 'Ankka', 70]]
    let sql = 'INSERT INTO registration (first, last, age) VALUES ?';

    db.query(sql, [values], (error, result) => {
        if (error) throw error;
        res.send({message: 'Data inserted.'});
    });
});


//Select all data
app.get('/api/registrations', (req, res)=> {
    let sql = 'SELECT * FROM registration';
    db.query(sql, (error, results) => {
        if (error) throw error;
        res.send({error: false, data: results, message: 'Registration list'});
    });
});

//Select specific data
app.get('/api/registrations/:id', (req, res)=> {
    let id = req.params.id;
    if (!id) {
        return res.status(400).send({ error: true, message: 'Please provide user id' });
       };
    let sql = 'SELECT * FROM registration WHERE id = ?'
    db.query(sql, [id], (error, results) => {
        if (error) throw error;
        return res.send({ error: false, data: results[0], message: 'Registration fetched' });
    });
});

//Method for inserting data
app.post('/api/registration', function (req, res) {
    let firstname = req.body.first;
    let lastname = req.body.last;
    let personage = req.body.age;
    let post = {first : firstname, last : lastname, age : personage};
    let sql = 'INSERT INTO registration SET ? ';

    if (!firstname || !lastname || !personage) {
      return res.status(400).send({ error:true, message: 'Please provide registration info' });
    }
    db.query(sql, [post],(error,result) => {
        if (error) throw error;
        return res.send({ error: false, data: result, message: 'New registration has been inserted successfully.' });
    });

});

//Update data
app.put('/api/registration', function (req, res) {
    let personid = req.body.id;
    let firstname = req.body.first;
    let lastname = req.body.last;
    let personage = req.body.age;
    let post = {id: personid};
    if(firstname){
        post["first"]=firstname;
    }
    if(lastname){
        post["last"]=lastname;
    }
    if(personage){
        post["age"]=personage;
    }
    let sql = "UPDATE registration SET ? WHERE id = ?";

    if (!personid && !firstname && !lastname && !personage) {
      return res.status(400).send({ error:true, message: 'Please provide registration info to be updated' });
    }
    if (!personid) {
        return res.status(400).send({ error:true, message: 'Please provide user ID'});
      }
   
    db.query(sql, [post, personid], function (error, results) {
      if (error) throw error;
      return res.send({ error: false, data: results, message: 'Registration has been updated successfully.' });
     });
    });

//Delete 
app.delete('/api/registration/:id', function (req, res) {
    let personid = req.params.id;
    let sql = 'DELETE FROM registration WHERE id = ?'
    if (!personid) {
        return res.status(400).send({ error: true, message: 'Please provide id' });
    }
    db.query(sql, [personid], function (error, results) {
        if (error) throw error;
        return res.send({ error: false, data: results, message: 'Registration has been deleted successfully.' });
    });
    }); 

app.listen('3001', () => {
    console.log('Server started on port 3001.');
});

module.exports = app;
