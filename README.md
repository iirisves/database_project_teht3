Made by Iiris Vesala. For demo purposes only.

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## To run locally
### Install MySQL on your local computer
Make sure you have installed MySQL (or a free fork of SQL, like MariaDB) and a MySQL client (like HeidiSQL, which comes bundled together with the MariaDB download for Windows) on your computer.
[Download MariaDB & HeidiSQL] (https://downloads.mariadb.org/)
### Connect to your local MySQL session as root user
Connect to your MySQL client locally as user = root, with no password (i.e. password = ' ').
Then create a database called "db".

This is because these are the connection details hardcoded in file server.js. This app is for demo purposes only and similar code should not be used in production.
After that is done, run:
`cd src`
`npm install`
`nodemon server.js`
This will make the server run on port 3001.

Open another terminal and run:
`cd src`
`npm start`
This should open https://localhost/3000 on your browser.

Once the app has opened, navigate to https://localhost/3000/howto to see the instructions for populating the database.